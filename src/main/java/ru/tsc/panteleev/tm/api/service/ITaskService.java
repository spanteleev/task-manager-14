package ru.tsc.panteleev.tm.api.service;

import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAll(Sort sort);

    List<Task> findAllByProjectId(String projectId);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);

    Task add(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusById(String id, Status status);

}
