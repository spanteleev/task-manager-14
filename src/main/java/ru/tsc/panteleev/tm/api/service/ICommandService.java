package ru.tsc.panteleev.tm.api.service;

import ru.tsc.panteleev.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
